/* main.c
 *
 * Copyright 2019 Mike
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "keyboard-config.h"
#include "keyboard-window.h"
#include <string.h>
#include <assert.h>
//const gchar *key;

/* static void window_setting(GtkWidget *button, gpointer data){ */
/*                //data1.values[0] = 0; */
/* 		          // ret = ioctl(req.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data1); */
               /* fdSetting = g_object_ref (windowSetting); */
               /* gtk_container_remove (GTK_CONTAINER (windowApplication),windowSetting); */
               /* gtk_container_add(GTK_CONTAINER(windowApplication), data); */
               /* gtk_widget_show_all(windowApplication); */

/*                key = gtk_button_get_label (data); */
/*                  printf("k et %s",key); */

/* } */

  GtkWidget *windowConnexion;
  GtkWidget *entryUsr;
  char str[20]="";
  char svg[20]="";
  GtkWidget *fdAcceuil;
  GtkWidget *fdConnexion;
static void clavier (GtkWidget *widget,gpointer   data)
{
//  g_print ("%s",gtk_button_get_label (data));
     strcpy(svg, gtk_entry_get_text (GTK_ENTRY(entryUsr)));
     strcpy(str, gtk_button_get_label (data));
     strcat(svg,str);
     gtk_entry_set_text (GTK_ENTRY(entryUsr),svg);
 // g_print ("%s",str);
  //str =

}

static void button_del (GtkWidget *widget,
             gpointer   data)
{
    strcpy(svg,"");
    gtk_entry_set_text (GTK_ENTRY(entryUsr),svg);
//     strcat(str,gtk_button_get_label (data));
//  gtk_entry_set_text (GTK_ENTRY(entryUsr),str);

}

static void button_ok (GtkWidget *widget,
             gpointer   data)
{
  if (strcmp(svg, "4444") == 0){
    // printf("You like cheese too!");
    g_print ("Right\n");
  }
  else{
    g_print ("Failed\n");

  }
  strcpy(svg,"");
  gtk_entry_set_text (GTK_ENTRY(entryUsr),svg);

}
/*Rezise image*/
GtkWidget *imageSize(GdkPixbuf *Im, int des_width, int des_height)
{
	GdkPixbuf *imagepixbuf;
	GError *error = NULL;
	assert(error == NULL);
	/*input GdkPixbul return GdkPixbuf */
	imagepixbuf = gdk_pixbuf_scale_simple(Im, des_width, des_width, GDK_INTERP_BILINEAR);

	/*input GdkPixbuf and Return GtkWidget  */
	return gtk_image_new_from_pixbuf(imagepixbuf);
}

GtkWidget *ui_connexion(GtkWidget *fixed,GtkWidget *button2, GtkWidget *usr,GtkWidget *im)
{


	GtkWidget *label1;
	GtkWidget *bn;
	label1 = gtk_label_new("PASSWORD");
	gtk_fixed_put(GTK_FIXED(fixed), label1, 430, 325);
	gtk_fixed_put(GTK_FIXED(fixed), usr, 515, 310);
  gtk_widget_set_size_request(usr, 260, 40);
	/*Button set */
	gtk_fixed_put(GTK_FIXED(fixed), button2, 100, 150);
	gtk_widget_set_size_request(button2, 200, 50);
	bn = gtk_button_new_with_label(NULL);
	gtk_button_set_relief(GTK_BUTTON(bn), GTK_RELIEF_NORMAL);
	gtk_button_set_image(GTK_BUTTON(bn), im);
	/*Button position */
	gtk_fixed_put(GTK_FIXED(fixed), bn, 515, 20);
	gtk_widget_set_size_request(bn, 250, 250);


	return fixed;
}
static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *table1;
  GtkWidget *grid;
  GtkWidget *table2;
	GError *error = NULL;
  GtkWidget *imageIUCConnexion;

                 	/*windowconnexion declaration widget */
	GtkWidget *buttonCancel;
  GtkWidget *buttons[16];

  window = gtk_application_window_new (app);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 1280, 800);
  gtk_window_set_title(GTK_WINDOW(window), "GtkTable");
  gchar *values[16] = {
     "7", "8", "9", "A",
     "4", "5", "6", "B",
     "1", "2", "3", "C",
     "DEL", "0", "F1", "OK"
  };
	//entryPwd = gtk_entry_new();
	entryUsr = gtk_entry_new();
  gtk_entry_set_max_length (GTK_ENTRY (entryUsr),8);

  /*Set button connexion screen */
	buttonCancel = gtk_button_new_with_label("CANCEL");
  windowConnexion = gtk_fixed_new();

  table1 = gtk_grid_new ();
  grid = gtk_grid_new ();
  table2 = gtk_grid_new ();

  gtk_grid_set_column_homogeneous (GTK_GRID(grid),TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID(grid),TRUE);
  gtk_grid_set_column_homogeneous (GTK_GRID(table1),TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID(table1),TRUE);
  gtk_grid_set_row_spacing(GTK_GRID(table1), 4);
  gtk_grid_set_column_spacing (GTK_GRID (table1), 4);

  int i = 0;
  int j = 0;
  int pos = 0;

  for (i=0; i < 4; i++) {
    for (j=0; j < 4; j++) {
      buttons[pos] = gtk_button_new_with_label(values[pos]);
      gtk_grid_attach(GTK_GRID(table1), buttons[pos], j, i, 1,1);
      pos++;
    }
  }

  imageIUCConnexion = imageSize(gdk_pixbuf_new_from_file("/home/mike/Projects/keyboard/src/iuc2.png", &error), 250, 250);

  windowConnexion = ui_connexion(windowConnexion,buttonCancel, entryUsr, imageIUCConnexion);


   gtk_grid_attach(GTK_GRID(grid), table1, 0, 1, 2,1);
   gtk_grid_attach(GTK_GRID(grid), table2, 0, 0, 2,1);
   gtk_grid_attach(GTK_GRID(table2), windowConnexion, 0, 0, 2,1);


  gtk_container_add(GTK_CONTAINER(window), grid);
 //  gtk_container_add(GTK_CONTAINER(window), windowConnexion);

                  /*Action of user in home windowConnexion*/
//   g_signal_connect(buttonCancel, "clicked",G_CALLBACK(window_connexion), (gpointer)fdAcceuil);

               /*Action of connexion button*/
//   g_signal_connect(buttonConnexion, "clicked",G_CALLBACK(window_connexion), (gpointer)fdHome);


  g_signal_connect(buttons[0], "clicked",G_CALLBACK(clavier),(gpointer)buttons[0]);
  g_signal_connect(buttons[1], "clicked",G_CALLBACK(clavier),(gpointer)buttons[1]);
  g_signal_connect(buttons[2], "clicked",G_CALLBACK(clavier),(gpointer)buttons[2]);
  g_signal_connect(buttons[3], "clicked",G_CALLBACK(clavier),(gpointer)buttons[3]);
  g_signal_connect(buttons[4], "clicked",G_CALLBACK(clavier),(gpointer)buttons[4]);
  g_signal_connect(buttons[5], "clicked",G_CALLBACK(clavier),(gpointer)buttons[5]);
  g_signal_connect(buttons[6], "clicked",G_CALLBACK(clavier),(gpointer)buttons[6]);
  g_signal_connect(buttons[7], "clicked",G_CALLBACK(clavier),(gpointer)buttons[7]);
  g_signal_connect(buttons[8], "clicked",G_CALLBACK(clavier),(gpointer)buttons[8]);
  g_signal_connect(buttons[9], "clicked",G_CALLBACK(clavier),(gpointer)buttons[9]);
  g_signal_connect(buttons[10], "clicked",G_CALLBACK(clavier),(gpointer)buttons[10]);
  g_signal_connect(buttons[11], "clicked",G_CALLBACK(clavier),(gpointer)buttons[11]);
  g_signal_connect(buttons[13], "clicked",G_CALLBACK(clavier),(gpointer)buttons[13]);

  /*DELL and OK button*/
  g_signal_connect(buttons[12], "clicked",G_CALLBACK(button_del),(gpointer)buttons[12]);
  g_signal_connect(buttons[15], "clicked",G_CALLBACK(button_ok),(gpointer)buttons[15]);

//  g_signal_connect_after (buttons[0], "clicked",G_CALLBACK(print_char),(gpointer)buttons[0]);
  gtk_widget_show_all(window);

}



int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.keyboard.Builder", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}

