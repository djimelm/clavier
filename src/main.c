/* main.c
 *
 * Copyright 2019 Mike
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "keyboard-config.h"
#include "keyboard-window.h"
#include <string.h>
#include <assert.h>
//const gchar *key;

/* static void window_setting(GtkWidget *button, gpointer data){ */
/*                //data1.values[0] = 0; */
/* 		          // ret = ioctl(req.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data1); */
               /* fdSetting = g_object_ref (windowSetting); */
               /* gtk_container_remove (GTK_CONTAINER (windowApplication),windowSetting); */
               /* gtk_container_add(GTK_CONTAINER(windowApplication), data); */
               /* gtk_widget_show_all(windowApplication); */

/*                key = gtk_button_get_label (data); */
/*                  printf("k et %s",key); */

/* } */

  GtkWidget *windowKey;
  GtkWidget *entryKey;
  char str[20]="";
  char svg[20]="";
  char *entete = "ENTRER LE NIP";
  GtkWidget *fdAcceuil;
  GtkWidget *fdConnexion;
static void clavier_key (GtkWidget *widget,gpointer   data)
{
//  g_print ("%s",gtk_button_get_label (data));
     strcpy(svg, gtk_entry_get_text (GTK_ENTRY(entryKey)));
     strcpy(str, gtk_button_get_label (data));
     strcat(svg,str);
     gtk_entry_set_text (GTK_ENTRY(entryKey),svg);
 // g_print ("%s",str);
  //str =

}

static void key_del (GtkWidget *widget,
             gpointer   data)
{
    strcpy(svg,"");
    gtk_entry_set_text (GTK_ENTRY(entryKey),svg);
//     strcat(str,gtk_button_get_label (data));
//  gtk_entry_set_text (GTK_ENTRY(entryEdit),str);

}

static void key_ok (GtkWidget *widget,
             gpointer   data)
{
  if (strcmp(svg, "4444") == 0){
    // printf("You like cheese too!");
    g_print ("Right\n");
  }
  else{
    g_print ("Failed\n");

  }
  strcpy(svg,"");
  gtk_entry_set_text (GTK_ENTRY(entryKey),svg);

}

GtkWidget *ui_key(GtkWidget *fixed,GtkWidget *button2, GtkWidget *usr,gchar *txt)
{


	GtkWidget *label1;
	label1 = gtk_label_new(NULL);
  gtk_label_set_text(GTK_LABEL (label1),txt);
	gtk_fixed_put(GTK_FIXED(fixed), label1, 515, 130);
	gtk_fixed_put(GTK_FIXED(fixed), usr, 515, 160);
  gtk_widget_set_size_request(usr, 260, 50);
	/*Button set */
	gtk_fixed_put(GTK_FIXED(fixed), button2, 100, 160);
	gtk_widget_set_size_request(button2, 200, 50);
	return fixed;
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *table1Key;
  GtkWidget *gridKey;
  GtkWidget *table2Key;

                 	/*windowconnexion declaration widget */
	GtkWidget *buttonCancelKey;
  GtkWidget *buttonsKey[16];

  window = gtk_application_window_new (app);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 1280, 800);
  gtk_window_set_title(GTK_WINDOW(window), "GtkTable");
  gchar *valuesUser[16] = {
     "7", "8", "9", "A",
     "4", "5", "6", "B",
     "1", "2", "3", "C",
     "DEL", "0", "F1", "ENTER"
  };
	//entryPwd = gtk_entry_new();
	entryKey = gtk_entry_new();
  gtk_entry_set_max_length (GTK_ENTRY (entryKey),6);

  /*Set button connexion screen */
	buttonCancelKey = gtk_button_new_with_label("CANCEL");
  windowKey = gtk_fixed_new();

  table1Key = gtk_grid_new ();
  gridKey = gtk_grid_new ();
  table2Key = gtk_grid_new ();

  gtk_grid_set_column_homogeneous (GTK_GRID(gridKey),TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID(gridKey),TRUE);
  gtk_grid_set_column_homogeneous (GTK_GRID(table1Key),TRUE);
  gtk_grid_set_row_homogeneous (GTK_GRID(table1Key),TRUE);
  gtk_grid_set_row_spacing(GTK_GRID(table1Key), 4);
  gtk_grid_set_column_spacing (GTK_GRID (table1Key), 4);

  int i = 0;
  int j = 0;
  int pos = 0;

  for (i=0; i < 4; i++) {
    for (j=0; j < 4; j++) {
      buttonsKey[pos] = gtk_button_new_with_label(valuesUser[pos]);
      gtk_grid_attach(GTK_GRID(table1Key), buttonsKey[pos], j, i, 1,1);
      pos++;
    }
  }
  pos=0;


  windowKey = ui_key(windowKey,buttonCancelKey, entryKey,&entete);


   gtk_grid_attach(GTK_GRID(gridKey), table1Key, 0, 1, 2,1);
   gtk_grid_attach(GTK_GRID(gridKey), table2Key, 0, 0, 2,1);
   gtk_grid_attach(GTK_GRID(table2Key), windowKey, 0, 0, 2,1);


  gtk_container_add(GTK_CONTAINER(window), gridKey);
 //  gtk_container_add(GTK_CONTAINER(window), windowEdit);

                  /*Action of user in home windowEdit*/
//   g_signal_connect(buttonCancel, "clicked",G_CALLBACK(window_connexion), (gpointer)fdAcceuil);

               /*Action of connexion button*/
//   g_signal_connect(buttonConnexion, "clicked",G_CALLBACK(window_connexion), (gpointer)fdHome);


  g_signal_connect(buttonsKey[0], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[0]);
  g_signal_connect(buttonsKey[1], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[1]);
  g_signal_connect(buttonsKey[2], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[2]);
  g_signal_connect(buttonsKey[3], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[3]);
  g_signal_connect(buttonsKey[4], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[4]);
  g_signal_connect(buttonsKey[5], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[5]);
  g_signal_connect(buttonsKey[6], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[6]);
  g_signal_connect(buttonsKey[7], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[7]);
  g_signal_connect(buttonsKey[8], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[8]);
  g_signal_connect(buttonsKey[9], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[9]);
  g_signal_connect(buttonsKey[10], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[10]);
  g_signal_connect(buttonsKey[11], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[11]);
  g_signal_connect(buttonsKey[13], "clicked",G_CALLBACK(clavier_key),(gpointer)buttonsKey[13]);

  /*DELL and OK button*/
  g_signal_connect(buttonsKey[12], "clicked",G_CALLBACK(button_del),(gpointer)buttonsKey[12]);
  g_signal_connect(buttonsKey[15], "clicked",G_CALLBACK(button_ok),(gpointer)buttonsKey[15]);

//  g_signal_connect_after (buttonsKey[0], "clicked",G_CALLBACK(print_char),(gpointer)buttons[0]);
  gtk_widget_show_all(window);

}



int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.keyboard.Builder", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}

